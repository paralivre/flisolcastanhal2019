# FLISOL 2019 em Castanhal

Projeto destinado a organização do evento Festival Latino-americano de Instalação de Software Livre FLISol que será realizado no dia 27 de Abril de 2019 na cidade de Castanhal (PA).

# O que é FLISOL?

O Festival Latino-americano de Instalação de Software Livre (FLISoL) é o maior evento da América Latina de divulgação de Software Livre. Ele é realizado desde o ano de 2005, e desde 2008 sua realização acontece no 4o. sábado de abril de cada ano.
Seu principal objetivo é promover o uso de Software Livre, mostrando ao público em geral sua filosofia, abrangência, avanços e desenvolvimento.
Para alcançar estes objetivos, diversas comunidades locais de Software Livre (em cada país/cidade/localidade), organizam simultâneamente eventos em que se instala, de maneira gratuita e totalmente legal, Software Livre nos computadores dos participantes. Além disso, paralelamente acontecem palestras, apresentações e workshops, sobre temas locais, nacionais e latino-americanos sobre Software Livre, em toda a sua expressão: artística, acadêmica, empresarial e social.
O FLISOL Brasil 2019 acontece no dia 27 de Abril em diversas cidades.

# Site

https://flisol.info/FLISOL2019/Brasil/Castanhal